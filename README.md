# RTSP to websocket

Proof of Concept of IP camera streaming to websocket with Node.js [node-rtsp-stream](https://www.npmjs.com/package/node-rtsp-stream) (server-side) and [jsmpeg](https://github.com/phoboslab/jsmpeg) (client-side).

It works really well but picture is ugly.

## How does it work?

Stream server receives the RTSP stream from camera and send it to websocket. Web server only provides simple web page which serves only as a client which connects to websocket and shows the stream.

If more people open the webpage, webpage always connect to the same websocket, so there is still only one RTSP stream received from camera so it saves camera HW and bandwidth between camera and stream server.

## Sources
### Looking for sources
https://www.google.com/search?newwindow=1&client=ubuntu&hs=7yl&channel=fs&ei=aT55XNH0EYfTwALf-pSYBw&q=rtsp+html5+player+over+websocket&oq=rtsp+html5+player+over+websocket&gs_l=psy-ab.3...191236918.191238785..191239621...0.0..0.71.327.5......0....1..gws-wiz.......0i71.z58CsKQWtzg
https://www.google.com/search?q=rtsp+to+webrtc&oq=rtsp+to+we&aqs=chrome.0.0j69i57j0l4.2305j0j7&sourceid=chrome&ie=UTF-8

### Used sources
https://medium.com/@chpmrc/how-to-stream-rtsp-on-the-web-using-web-sockets-and-canvas-d821b8f7171e
https://www.npmjs.com/package/node-rtsp-stream
https://github.com/phoboslab/jsmpeg

### Other found sources
https://devhub.io/repos/kyriesent-node-rtsp-stream
http://webrtc.live555.com/
https://webrtc.github.io/samples/
https://github.com/webrtc/samples
https://www.quora.com/How-do-I-embed-RTSP-into-HTML5
https://www.npmjs.com/package/streamedian
https://github.com/linkingvision/h5stream

## Nice to have
- mpeg1 is ugly, use some lib with h264 support
- add control of RTSP stream (play, pause, etc.), where should be a buffer?
- make a test with camera disconnection, slow link, etc. > is it robust enough?
