Stream = require('node-rtsp-stream');

const cam_user = process.env.CAM_USER;
cam_pass = process.env.CAM_PASS;
cam_url = process.env.CAM_URL;
cam_rtsp_url = 'rtsp://' + cam_user + ':' + cam_pass + '@' + cam_url;

stream = new Stream({
  name: 'name',
  streamUrl: cam_rtsp_url,
  wsPort: process.env.PORT,
  ffmpegOptions: { // options ffmpeg flags
    '-f': 'mpegts',
    '-c:v': 'mpeg1video',
    '-bf': '0',
    '-c:a': 'mp2',
    '-stats': '', // an option with no neccessary value uses a blank string
    '-r': 30 // options with required values specify the value after the key
  }
});
